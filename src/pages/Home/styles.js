import styled from 'styled-components/native';

import LinearGradient from 'react-native-linear-gradient';

export const Container = styled(LinearGradient).attrs({
  colors: ['#4c669f', '#3b5998', '#192f6a']
})`
  flex: 1;
  align-items: center;
  justify-content: center;
  background-color: #0499ac;
`;

export const Title = styled.Text`
  color: #FFF;
  font-size: 30px;
  font-weight: bold;
`;

export const Button = styled.TouchableOpacity`
  background-color: #00BFFF;
  border-radius: 3px;
  height: 50px;
  padding: 0px 20px;
  margin-top: 10px;
  border-radius: 20px;
  justify-content: center;
  align-items: center;
`;

export const ButtonText = styled.Text`
  color: #FFF;
  font-size: 18px;
  font-weight: bold;
`;