import React from 'react';

import { Container, Title, Button, ButtonText } from './styles'

export default function Home() {
  return (
    <Container>
     <Title>Home - Modulo Seguros</Title>
     <Button onPress={() => alert('eae')}>
       <ButtonText>Ir para a Settings</ButtonText>
     </Button>
   </Container>
  );
}
